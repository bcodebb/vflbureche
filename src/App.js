import './App.css';
import VFL from './components/vfl';

function App() {
  return (
    <div className="App">
      <VFL />
    </div>
  );
}

export default App;
