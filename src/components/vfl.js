import "@babel/polyfill";
import React, { useState, useRef, useEffect } from 'react';
import { 
  CssBaseline,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
  Slide,
  Link,
} from '@material-ui/core';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';
import { makeStyles } from '@material-ui/core/styles';
import { 
  mdiChevronRightCircle,
  mdiChevronLeftCircle,
} from '@mdi/js';
import { Icon } from '@mdi/react';
import ReactPlayer from 'react-player';
import { videos } from '../const/videos';

import overlayBg from '../imgs/IMG_0029.png';
import burecheLogo from '../imgs/logo-bureche.png';

// USO DE ESTILOS
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  vfl: {
    overflow: "hidden",
  },
  player: {
    position: 'absolute',
    overflow: 'hidden',
  },
  overlay: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    height: 200,
  },
  overlayText: {
    backgroundImage: `url('${overlayBg}')`,
    backgroundPosition: "center",
    backgroundSize: "75% 175%",
    fontFamily: "Roboto Slab",
    color: "#fff",
    fontSize: 40,
  },
  drawerPaper: {
    background: "#eee",
    color: "#e20612",
  },
  drawerLogo: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "95%",
    marginTop: -25,
  },
  mainLogo: {
    position: "absolute",
    top: 25,
    right: 25,
    width: "25%",
    [theme.breakpoints.up('md')]: {
      width: "10%",
    },
  },
  list: {
    width: 250,
    marginTop: 225,
  },
  listItem: {
    "& .MuiListItemIcon-root": {
      color: "#e20612",
    },
    "&:hover": {
      background: "#e20612",
      color: "#fff",
      "& .MuiListItemIcon-root": {
        color: "#fff",
      },
    },
  },
  listItemText: {
    fontFamily: "Roboto Slab",
  },
  menu: {
    position: "absolute",
    cursor: "pointer",
    left: "10px",
    [theme.breakpoints.up('md')]: {
      left: "20px",
    },
    top: "45%",
    '&:focus': {
      outline: 'none',
    },
    '&:selected': {
      outline: 'none',
    },
    '&:hover': {
      outline: 'none',
    },
  },
  menuText: {
    textAlign: "center",
    fontFamily: "Roboto Slab",
    color: "#fff",
  },
})); 

// COMPONENTE VIDEOFRAMELOOP
const VFL = () => {
  const classes = useStyles();
  const [video, setVideo] = useState(videos[0]);
  const [open, setOpen] = useState(false);
  const [overlay, setOverlay] = useState(false);
  const [sbOpener, setSbOpener] = useState(false);
  const [showLogo, setShowLogo] = useState(true);

  const ref = useRef();

  const handleLayoutStuff = () => {
    setSbOpener(true);
    setOverlay(true);
    setTimeout(() => {
      setOverlay(false);
    }, 3000);
  };

  const handleVideoLoop = video => {
    setVideo(video);
    setSbOpener(false);
  };

  const handlePauseVideo = () => setShowLogo(true);
  const handlePlayVideo = () => setShowLogo(false);

  return (
    <div className={classes.vfl}>
      <CssBaseline />

      {/* Sidebar */}
      <SwipeableDrawer
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor='left'
        open={open}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
      >
        <Link href="https://colegiobureche.edu.co"><img src={burecheLogo} className={classes.drawerLogo} /></Link>
        <div className={classes.list} role="presentation" onClick={() => setOpen(false)} onKeyDown={() => setOpen(false)}>
          <List>
            {videos.map(video => (
              <ListItem button key={video.id} className={classes.listItem} onClick={() => handleVideoLoop(video)}>
                <ListItemIcon>
                  <SlowMotionVideoIcon />
                </ListItemIcon>
                <ListItemText classes={{ primary: classes.listItemText }} primary={video.name} />
              </ListItem>
            ))}
          </List>
        </div>
      </SwipeableDrawer>

      {/* Reproductor ocupando toda la pantalla */}
      <ReactPlayer
        className={classes.player}
        url={video.link}
        loop={true}
        width='100%'
        height='100%'
        controls={true}
        playing={true}
        onStart={handleLayoutStuff}
        onPause={handlePauseVideo}
        onPlay={handlePlayVideo}
        muted
        playsInline
        webkitPlaysInline
        config={{
          youtube:{
            playerVars:{
              rel: 0,
              disablekb: 1,
              autoplay: 1,
            }
          }
        }}
      />

      {/* Identificación del vídeo 360 en pantalla */}
      <Slide direction='up' in={overlay} mountOnEnter unmountOnExit timeout={1000}>
        <div className={classes.overlay}>
          <Typography variant="h3" color="secondary" className={classes.overlayText}>{video.name}</Typography>
        </div>
      </Slide>

      {/* Identificación del vídeo 360 en pantalla */}

      {/* Botón de menú */}
      <Slide direction='right' in={sbOpener} mountOnEnter unmountOnExit timeout={500}>
        <div className={classes.menu} onClick={() => setOpen(true)}>
          <Typography variant="h3" color="secondary" className={classes.menuText}>
            <Icon path={sbOpener ? mdiChevronRightCircle : mdiChevronLeftCircle} size={2} title="Open Sidebar" />
          </Typography>
        </div>
      </Slide>

      {/* Logo dinámico */}
      <Slide direction='left' in={showLogo} mountOnEnter unmountOnExit timeout={500}>
        <img src={burecheLogo} className={classes.mainLogo} />
      </Slide>
    </div>
  );
};

export default VFL;