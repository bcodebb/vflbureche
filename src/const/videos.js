export const videos = [
    { id: 1, name: 'Arenero', link: 'https://www.youtube.com/watch?v=OnAfajpvuDo' },
    { id: 2, name: 'Biblioteca', link: 'https://www.youtube.com/watch?v=SacbHjZwHlE' },
    { id: 3, name: 'Salón de Arte', link: 'https://www.youtube.com/watch?v=P9uuYLOpwzI' },
    { id: 4, name: 'Bachillerato', link: 'https://www.youtube.com/watch?v=1kxXBbGmHrQ' },
    { id: 5, name: 'Kinder 1-4', link: 'https://www.youtube.com/watch?v=p1mzDSuaD38' },
    { id: 6, name: 'Kinder 5', link: 'https://www.youtube.com/watch?v=TbZfEbtMwQU' },
    { id: 7, name: 'Granja', link: 'https://www.youtube.com/watch?v=aDk0mqRU-RM' },
]

// link: 'https://www.youtube.com/watch?v=OnAfajpvuDo'